from django.shortcuts import render
from statuspage.models import Status

response = {}

def index(request):
	number_of_status = Status.objects.all().count()
	response['total'] = number_of_status
	if response['total'] != 0:
		last_post = Status.objects.latest("created_date")
		response['last'] = last_post
	return render(request, 'stats.html', response)


from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from statuspage.models import Status 

# Create your tests here.

class StatsUnitTest(TestCase):
	
	def test_status_page_url_is_exist(self):
		response = Client().get('/stats/')
		self.assertEqual(response.status_code,200)

	def test_status_page_using_index_func(self):
		found = resolve('/stats/')
		self.assertEqual(found.func, index)

	def test_number_of_post_already_in_dashboard(self):
		new_post = Status.objects.create(new_status = "test")
		number_of_post = str(Status.objects.all().count())
		response = Client().get('/stats/')
		html_response = response.content.decode('utf8')
		self.assertIn(number_of_post, html_response)



from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from .models import Database


# Create your tests here.


class ProfilePageUnitTest(TestCase):

    def test_lab_4_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_html_is_good(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode("utf-8")

        self.assertIn("hepzibah smithies", html_response)
        self.assertIn("19-02-1998", html_response)
        self.assertIn("tranquilt", html_response)
        self.assertIn("the best hepzibah ever", html_response)
        self.assertIn("hepzibah@smithies.joewono.com", html_response)
    
    def test_profile_test(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode("utf-8")

        self.assertIn("python", html_response)
        self.assertIn("runserver8000", html_response)
        self.assertIn("manage.py", html_response)
       